<?php 
function get_type($var) 
{ 
    if(is_object($var)) 
        return get_class($var); 
    if(is_null($var)) 
        return 'null'; 
    if(is_string($var)) 
        return 'string'; 
    if(is_array($var)) 
        return 'array'; 
    if(is_int($var)) 
        return 'integer'; 
    if(is_bool($var)) 
        return 'boolean'; 
    if(is_float($var)) 
        return 'float'; 
    if(is_resource($var)) 
        return 'resource'; 
    //throw new NotImplementedException(); 
    return 'unknown'; 
} 

class DBMS_SqLite extends  DBMS
{ 
  protected $fieldType__ = array();
  protected function connect_($connString)
  { 
    $this->engine = 'sqlite';
  //echo "TRACE [connect_] connString : ".$connString."<br>";
    $this->connection = new SQLite3($connString);
    //retrieve the create statement query for the source table; 
    $sourcetbl_create_statement = $this->connection->querySingle(
      "select sql from sqlite_master where type='table' and name='my_template'"); 
    if ($sourcetbl_create_statement===false) die($this->connection->lastErrorMsg()); 
    return(1);    
  }
  public function ExecQuery($query_)
  {
    return($this->connection->exec($query_));
  }  
  protected function getQueryResult_($query_)
  {
  //echo "TRACE [getQueryResult_] 1 <br>";
    $this->result = $this->connection->query($query_);
    if($this->result instanceof Sqlite3Result) 
    { 
    //echo "TRACE [getQueryResult_] 2 <br>";
      $this->queryOk = 1; 
      $row = $this->result->fetchArray(SQLITE3_NUM);
      for ($colnum=0; $colnum<count($row); $colnum++) 
      {
        $fieldId = $this->result->columnName($colnum);
        $dt = stripos($fieldId,"dt_");    
        $dd = stripos($fieldId,"dd_");    
        $dh = stripos($fieldId,"dh_");
      //echo "TRACE [getQueryResult_] 2.1 colnum=".$colnum." fieldId=".$fieldId." dt=".$dt." dd=".$dd." dh=".$dh."<br>";
      //echo "TRACE [getQueryResult_] 2.2 tipeId=".$tipeId."<br>";
        if($dd!==false) { $this->fieldType__[$colnum] = 'date'; }
        else if(($dt!==false)||($dh!==false)) { $this->fieldType__[$colnum] = 'datetime'; }
        else { $this->fieldType__[$colnum] = get_type($row[$colnum]); }
      //echo "TRACE [getQueryResult_] 2.3.".$colnum." : ".$this->fieldType__[$colnum]." <br>";
      }      
      $this->reset_();
    }      
    else 
    { 
    //echo "TRACE [getQueryResult_] 3 <br>";
      $this->queryOk = 0; 
    }
  }
  protected function getFieldNumber_()
  {
    return($this->result->numColumns()); 
  }
  protected function getFieldName_($numField)
  {
    return($this->result->columnName($numField)); 
  }
  protected function getFieldType_($numField)
  {
    return($this->fieldType__[$numField]);
    /*
    $fieldId = $this->result->columnName($numField);
    $dt = stripos($fieldId,"dt_");    
    $dd = stripos($fieldId,"dd_");    
    $dh = stripos($fieldId,"dh_");
  //echo "TRACE [getQueryResult_] 1 numField=".$numField." fieldId=".$fieldId." dt=".$dt." dd=".$dd." dh=".$dh."<br>";
    $tipeId = $this->result->columnType($numField); 
  //echo "TRACE [getQueryResult_] 2 tipeId=".$tipeId."<br>";
         if(($dt!==false)||($dd!==false)||($dh!==false)) { return('datetime'); }
    else if($tipeId==SQLITE3_INTEGER) { return('number'); }
    else if($tipeId==SQLITE3_FLOAT) { return('number'); }
    else if($tipeId==SQLITE3_TEXT) { return('string'); }
    else if($tipeId==SQLITE3_BLOB) { return('blob'); }
    else if($tipeId==SQLITE3_NULL) { return('null'); }
    */
  }
  protected function reset_()
  {
    return($this->result->reset()); 
  }
  protected function finalize_()
  {
    return($this->result->finalize()); 
  }
  protected function fetchRecord_()
  {
    $this->fieldValues = $this->result->fetchArray(SQLITE3_NUM);
  }
  public function string_aggregate_fun()
  {
    return('group_concat');
  }
};
?>