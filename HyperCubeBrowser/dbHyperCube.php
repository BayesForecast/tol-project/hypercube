<?php
//HyperCubeBrowser
//CHECKOUT FROM SVN in htdocs
//svn checkout https://www.tol-project.org/svn/tolp/OfficialTolArchiveNetwork/HyperCube/HyperCubeBrowser HyperCubeBrowser
  $tabEngine = 'DataTable';
  include_once "general.php";
?>
<html>
<head>
  <meta charset="utf-8">
  <title>Bayes HyperCube Browser</title>
  <META http-equiv="Content-Script-Type" content="text/javascript">
  <script type="text/javascript" language="javascript" class="init">
  function divWidth() { <?php echo "return(".$width.");" ?> };
  function divHeight() { <?php echo "return(".$height.");" ?> }; 
  function projectIdentifier() { <?php echo "return('".$prj."');" ?> }; 
  </script>
  <script type="text/javascript" language="javascript" src="./tools.js">  </script>
  <script type="text/javascript" language="javascript" src="./mSelecTable.js">  </script>
  <script type="text/javascript" language="javascript" src="./seriesChart.js">  </script>
  <script type="text/javascript" language="javascript" src="./showInfo.js">  </script>
  <script type="text/javascript" language="javascript" src="./divPager.js">  </script>
  <script type="text/javascript" language="javascript" src="./HyperCubeBrowser.js">  </script>
</head>
<body class="HyperCubeBrowser">
  <div id=Credits style="background:linear-gradient(to left, rgba(0,0,0,0.05), rgba(10,10,10,.3)); position: absolute; width:5%; left: 0px; right: 0px; top: 0px; bottom: 0px;" > 
    <div id=LogoPrj>
      <IMG SRC="logo-O2.svg" HSPACE=5% VSPACE=10% WIDTH=90%>
    </div>
    <div id=LogoPrj style="position: absolute; bottom: 5%; left: 0;">
      <IMG SRC="bayes_hypercube_logo.gif" HSPACE=1% WIDTH=98% style="margin:auto">
    </div>
  </div>
  <div id=SepBar style="position: absolute; height:0.1%; left: 0px; right: 0px; top: 0px; bottom: 0px;" > </div>
  <div id=HyperCubeBrowserDiv style="position: absolute; width:95%; height:99.9%; left: 5%; right: 0px; top: 0.1%px; bottom: 0px;" > </div>
</body>
</html>
