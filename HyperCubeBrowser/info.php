<H2>TOL Hypercube</H2>
<p>
TOL Hypercube is a tool to handle with massivity and complexity of data in TOL language.
<p>
</p>
This tool includes TOL-inside diverse code utilities and this WEB-based application to exploit them.
</p>
<H3>Hypercube structure</H3>
<p>
TOL Hypercube data are stored in TOL objects of type Serie, Matrix, VMatrix or Set.
TOL objects are classified in data sets sharing the same source, concept and dimensional structure.
For each data set there are a sorted list of dimensions, eventually empty, forming a unique key in such a 
way that the name of the object will match this syntax rule 
</p>
<pre><code>
  &lt dataset &gt [. &lt dim_1 &gt . &lt dim_2 &gt . ��� . &lt dim_n &gt ]
</code></pre>
<H3>Interface manual</H3>
    
