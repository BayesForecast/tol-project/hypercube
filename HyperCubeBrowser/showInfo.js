/////////////////////////////////////////////////////////////////////////////
//FILE: showInfo.js
//
//PURPOSE: Defines Javascript class showInfo to handle with time series
//charting tools.
//
// -Data are charged with Ajax from a given URL
// -Chart is drawn in a DIV created into a present DIV main container
// -User gives a name to identify all related HTML elements that will be
//  created automatically: 
//   * #<name>Div
// 
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
var showInfo = function  (args) 
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE new showInfo');
//console.log(args);
  if (typeof(args.active)==='undefined') args.active = true;
  if (typeof(args.active)==='undefined') args.html = true;
//if (typeof(args.width)==='undefined') args.width = 800;
//if (typeof(args.height)==='undefined') args.height = 600;
  if (typeof(args.buttons)==='undefined') args.buttons = 
  ['csv','excel','copy','print'];
  if (typeof(args.enabledButtons)==='undefined') args.enabledButtons = [];
//console.log(args);
  this.active = args.active;
  this.divId = this.name+'Div';
//this.width = args.width;
//this.height = args.height;
  this.container = args.container;
  this.name = args.name;
  this.htmlContents = args.html;
  this.buttons = args.buttons;
//console.log(this);
};


/////////////////////////////////////////////////////////////////////////////
showInfo.prototype.html = function()
/////////////////////////////////////////////////////////////////////////////
{
  var div = document.getElementById(this.container);  
  var htm = 
  '<div id="'+this.divId+'" style="'+
  'width:100%; '+
  'height:100%; '+
  'display:'+(this.active?'block':'none')+
  '">\n';
  htm +=
  '<div  class="dt-buttons"'+
//'style="float:top; "'+
  '>\n';
  for(i=0; i<this.buttons.length; i++)
  {
    htm +=
    '<button class="dt-button" type="button" '+
    'onclick="clickPageButton(\''+this.buttons[i]+'\')">'+
    this.buttons[i]+'</button>';
  }  
  htm +=
  '<div id="'+this.divId+'Info" '+
  'style="border:solid,width:100%;height:100%;"'+
//'style="float: right;"'+
//'style="position: absolute; left: 0px; right: 0px; top: 40px; bottom: 0px;"'+
  '>\n'+ 
  this.htmlContents+
  '</div>\n';
//console.log('TRACE htm='+htm);
  return(htm);
};
 

/////////////////////////////////////////////////////////////////////////////
showInfo.prototype.show = function()
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE showInfo.prototype.show');
  this.active = true;
  $("#"+this.divId).show();
};

/////////////////////////////////////////////////////////////////////////////
showInfo.prototype.resize = function()
/////////////////////////////////////////////////////////////////////////////
{
//this.show();
};

/////////////////////////////////////////////////////////////////////////////
showInfo.prototype.hide = function()
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE showInfo.prototype.hide');
  this.active = false;
  $("#"+this.divId).hide();
};

/////////////////////////////////////////////////////////////////////////////
showInfo.prototype.onReady = function(env)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE showInfo.prototype.onReady('+this.name+')');
  var div = document.getElementById(this.container);
  div.insertAdjacentHTML('beforeend', this.html());
  document.getElementById(this.divId).style.fontSize = "16px";
//console.log(this_);
};

/////////////////////////////////////////////////////////////////////////////
showInfo.prototype.remove = function()
/////////////////////////////////////////////////////////////////////////////
{
  $("#"+this.divId).remove();  
};


