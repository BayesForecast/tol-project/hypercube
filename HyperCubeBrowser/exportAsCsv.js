  function dataTableToCSV(dataTable_arg) {
    var sc = ';';
    var scr = ',';
    var sl = "\r\n";
    //document.write("TRACE dataTableToCSV 1 <br>\n");
    var dt_cols = dataTable_arg.getNumberOfColumns();
    var dt_rows = dataTable_arg.getNumberOfRows();
    //document.write("TRACE dataTableToCSV 2 dt_cols : "+dt_cols+" <br>\n");
    //document.write("TRACE dataTableToCSV 3 dt_rows : "+dt_rows+" <br>\n");
    var csv_cols = [];
    var csv_out;
    // Iterate columns
    for (var i=0; i<dt_cols; i++) {
        // Replace any commas in column labels
        csv_cols.push(dataTable_arg.getColumnLabel(i).replace(sc,scr));
    }
    // Create column row of CSV
    csv_out = csv_cols.join(sc)+sl;
    // Iterate rows
    for (i=0; i<dt_rows; i++) {
        var raw_col = [];
        for (var j=0; j<dt_cols; j++) {
            // Replace any commas in row values
            raw_col.push(dataTable_arg.getFormattedValue(i, j, 'label').replace(sc,scr));
        }
        // Add row to CSV text
        csv_out += raw_col.join(sc)+sl;
    }
  //document.write("TRACE csv_out:"+csv_out);
    return csv_out;
  };
 

function saveAsWinCsv(csvContent, csvPath) 
{
  var textEncoder = new CustomTextEncoder('windows-1252', {NONSTANDARD_allowLegacyEncoding: true});
  var csvContentEncoded = textEncoder.encode([csvContent])
  var blob = new Blob([csvContentEncoded], {type: 'text/csv;charset=windows-1252'});
  saveAs(blob, csvPath);
};    

function export_click() 
{
  if(current_csv_prefix!='')
  {
  //alert("TRACE not empty current_csv_prefix:'"+current_csv_prefix+"'\n"+"summary_data='"+summary_data+"'\n");
    var csvContent = dataTableToCSV(window[current_csv_prefix+"_data"]);
    saveAsWinCsv(csvContent,current_csv_prefix+".csv");
  }
  else
  {
    document.write("empty current_csv_prefix:'"+current_csv_prefix+"'<br>\n");
  }
  
}
