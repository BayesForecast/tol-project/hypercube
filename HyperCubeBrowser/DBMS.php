<?php 
abstract class DBMS 
{ 
  public $engine = '';
  public $connectionOk = 0;
  protected $connectionString = '';
  protected $connection = 0;
  protected $query = '';
  public $queryOk = 0;
  protected $result = 0;
  public $fieldNumber = 0;
  public $fieldValues = array();
  public $fieldNames = array();
  public $fieldTypes = array();
  protected abstract function connect_($connString);
  protected abstract function getQueryResult_($query_);
  protected abstract function getFieldNumber_();
  protected abstract function getFieldName_($numField);
  protected abstract function getFieldType_($numField);
  protected abstract function reset_();
  protected abstract function finalize_();
  protected abstract function fetchRecord_();
  public abstract function string_aggregate_fun();
  public function Connect($connString)
  {
    $connectionString = $connString;
    if(!($this->connect_($connString)))
    {
      echo '<H1> Sorry: the data server is not respoding at this moment. '.
      'Try again after a few minutes </H1>\n';
      return(0);
    }
    return(1);
  }
  public abstract function ExecQuery($query_);  
  public function PrepareQuery($query_)
  {
  //echo "TRACE [PrepareQuery] 1 <br>";
    $this->query = $query_;
    $this->getQueryResult_($this->query);
    if(!($this->queryOk))
    {
      echo "ERROR invalid query_=<br>".$query_."<br>";
      return(0);
    }
  //echo "TRACE [PrepareQuery] 2 this->queryOk:".$this->queryOk."<br>";
    $this->fieldNumber = $this->getFieldNumber_();
    for($i=0; $i<$this->fieldNumber; $i++)
    {
      $this->fieldNames[$i] = $this->getFieldName_($i);
      $this->fieldTypes[$i] = $this->getFieldType_($i);      
    }
    $this->reset_();
  //echo "TRACE [PrepareQuery] 3 this->fieldNumber: ".$this->fieldNumber." <br>";
    return(1);
  }
  public function FetchRecord()
  {
    if(!($this->queryOk)) { return(0); }
    $this->fetchRecord_();
    if(!($this->fieldValues))
    {
      return(0);
    }
    $nf = count($this->fieldValues);   
    $ok = $nf == $this->fieldNumber;
  //echo "TRACE [FetchRecord] $ok=".$ok."$nf=".$nf." this->fieldNumber=".$this->fieldNumber."<br>";
    return($ok);
  }
  public function FetchTable($query_)
  {
  //echo "TRACE [FetchTable] 1 $query_=<br>".$query_."<br>";
    if(!$this->PrepareQuery($query_)) { return(0); }
  //echo "TRACE [FetchTable] 2 <br>";
    $table = array();
    $okReg = 0; 
    $i = 0;
    do { 
      $okReg = $this->FetchRecord();       
      if($okReg)
      {
        $table[$i] = $this->fieldValues; 
      //echo "TRACE [FetchTable] i=".$i." okReg=".$okReg; /*var_dump($table[$i]);*/ echo "<br>";
        $i = $i+1;
      }
    } while($okReg);
  //echo "TRACE [FetchTable] 3 <br>";
    $this->finalize_();
    return($table);
  }
};

include_once "DBMS_PostgreSql.php";
include_once "DBMS_SqLite.php";

function CreateDBMS($engine,$connStr)
{
  $dbms = 0;
  if($engine=='postgresql')
  {
    $dbms = new DBMS_PostgreSql();    
  }
  else if($engine=='sqlite')
  {
    $dbms = new DBMS_SqLite();    
  }
  if($dbms)
  {
    $dbms->Connect($connStr);
  }
  return($dbms);
};

?>