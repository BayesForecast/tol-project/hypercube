/////////////////////////////////////////////////////////////////////////////
//FILE: divPager.js
//
/////////////////////////////////////////////////////////////////////////////
/*
var divPage = function  (args) 
{
//console.log(args);
  if (typeof(args.active)==='undefined') args.active = false;
  this.pageHandler = args.pageHandler;
  this.active = args.active;
  //divId
};

divPage.prototype.show = function()
{
  this.active = true;
  this.pageHandler.show();
}; 

divPage.prototype.hide = function()
{
  this.active = false;
  this.pageHandler.hide();
}; 

divPage.prototype.onReady = function(env)
{
  this.pageHandler.onReady(env);
//if(this.active) { this.pageHandler.show(); }
//else            { this.pageHandler.hide(); }
}; 
*/

/////////////////////////////////////////////////////////////////////////////
var divPager = function  (args) 
/////////////////////////////////////////////////////////////////////////////
{
//console.log(args);
  if (typeof(args.activePage)==='undefined') args.activePage = '';
  this.container = args.container;
  this.activePage = args.activePage;
  this.current = "";
  this.pageIdx = {};
  this.pageObj = [];
  //divId
};

/////////////////////////////////////////////////////////////////////////////
divPager.prototype.existsPage = function(pageName)
/////////////////////////////////////////////////////////////////////////////
{
  return(!(typeof(this.pageIdx[pageName])==='undefined'));
}; 

/////////////////////////////////////////////////////////////////////////////
divPager.prototype.addPage = function(pgObj)
/////////////////////////////////////////////////////////////////////////////
{
  if(!this.existsPage(pgObj.name))
  {
    this.pageIdx[pgObj.name] = this.pageObj.length;
    this.pageObj.push(pgObj);    
  }
  else
  {
    var idx = this.pageIdx[pgObj.name];
    this.pageObj[idx].remove();
    this.pageIdx[pgObj.name] = idx;
    this.pageObj[idx] = pgObj;    
  }
}; 

/////////////////////////////////////////////////////////////////////////////
divPager.prototype.removePage = function(pageName)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE divPager.prototype.removePage('+pageName+')');
  if(this.existsPage(pageName))
  {
    var idx = this.pageIdx[pageName];
    this.pageObj[idx].remove();
  }
}; 

/////////////////////////////////////////////////////////////////////////////
divPager.prototype.getPage = function(pageName)
/////////////////////////////////////////////////////////////////////////////
{
  var idx = this.pageIdx[pageName];
  return(this.pageObj[idx]);
};

/////////////////////////////////////////////////////////////////////////////
divPager.prototype.show = function(pageName)
/////////////////////////////////////////////////////////////////////////////
{
//console.log("TRACE: divPager.prototype.show("+pageName+")");
//console.log(this);
  for (i=0; i< this.pageObj.length; i++)
  {
    this.pageObj[i].hide();
  }
  this.current = this.getPage(pageName);
  if (!(typeof(this.current)==='undefined'))
  {
    this.current.show();
  }
  else
  {
  //console.log("ERROR: divPager.prototype.show("+pageName+") Page not found!");
  }  
};
  
/////////////////////////////////////////////////////////////////////////////
divPager.prototype.resizeCurrent = function()
/////////////////////////////////////////////////////////////////////////////
{
//console.log("TRACE: divPager.prototype.resizeCurrent("+this.current.name+")");
  this.current.resize();
};
  
/////////////////////////////////////////////////////////////////////////////
divPager.prototype.onReady = function(env)
/////////////////////////////////////////////////////////////////////////////
{
//console.log("TRACE: divPager.prototype.onReady("+env+")");
  for (i=0; i< this.pageObj.length; i++)
  {
  //console.log(this.pageObj[i]);
    this.pageObj[i].onReady(env);
    if(this.pageObj[i].active) { this.current =this.pageObj[i]; }
  }
};
