<?php 
class DBMS_PostgreSql extends  DBMS
{ 
  protected function connect_($connString)
  { 
    $this->engine = 'postgresql';
  //echo "TRACE [connect_] connString : ".$connString."<br>";
    $this->connection = pg_connect($connString, PGSQL_CONNECT_FORCE_NEW)
    or die('Sorry: the POSTGRESQL data server is not respoding at this moment. '.
         'Try again after a few minutes' . pg_last_error());
    $this->connectionOk = 1;         
  //echo "TRACE [connect_] connectionOk : ".$this->connectionOk."<br>";
    return($this->connectionOk);
  }
  public function ExecQuery($query_)
  {
    return(pg_query($this->connection, $query_));
  }  
  protected function getQueryResult_($query_)
  {
    $this->result = pg_query($this->connection, $query_);
    if($this->result==0) { $this->queryOk = 0; }
    else { $this->queryOk = 1; }
  }
  protected function getFieldNumber_()
  {
    return(pg_num_fields($this->result)); 
  }
  protected function getFieldName_($numField)
  {
    return(pg_field_name($this->result,$numField)); 
  }
  protected function getFieldType_($numField)
  {
    return(pg_field_type($this->result,$numField)); 
  }
  protected function reset_()
  {
    return(pg_result_seek($this->result,0));
  }
  protected function finalize_()
  {
    return(pg_free_result($this->result));
  }
  protected function fetchRecord_()
  {
    $this->fieldValues = pg_fetch_row($this->result);
  }
  public function string_aggregate_fun()
  {
    return('string_agg');
  }
  
};
?>