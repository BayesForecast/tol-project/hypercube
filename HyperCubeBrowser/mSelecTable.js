/////////////////////////////////////////////////////////////////////////////
//FILE: mSelecTable.js
//
//PURPOSE: Defines Javascript class mSelecTable to handle with DataTable
//objects with multi-selection.
//
// -Data are charged with Ajax from a given URL
// -Table is drawn in a DIV created into a present DIV main container
// -User gives a name to identify all related HTML elements that will be
//  created automatically: 
//   * #<name>Div
//   * #<name>Tab
// -When a row or more are selected the specified key field is stored in a
//  string separated by comma. There is also an array of selected rows 
//  indexes and an array of full data in selected rows.
// 
/////////////////////////////////////////////////////////////////////////////

var jqueryVersion = '2.1.4';
var styler = 
  'bootstrap';
//'jqueryui';

document.write(
  '<link rel="stylesheet" href="./'+styler+'/css/'+styler+'.min.css">\n'+
  '<link rel="stylesheet" type="text/css" href="./DataTables/media/css/jquery.dataTables.min.css">\n'+
  '<link rel="stylesheet" type="text/css" href="./DataTables/media/css/dataTables.'+styler+'.min.css">\n'+
  '<link rel="stylesheet" type="text/css" href="./DataTables/extensions/Select/css/select.'+styler+'.min.css">\n'+  
  '<link rel="stylesheet" type="text/css" href="./DataTables/extensions/buttons/css/buttons.dataTables.min.css">\n'+
  '<link rel="stylesheet" type="text/css" href="./DataTables/extensions/buttons/css/buttons.'+styler+'.min.css">\n'+
  '<script type="text/javascript" src="./jquery-'+jqueryVersion+'.min.js" ></script>\n'+
  '<script type="text/javascript" src="./'+styler+'/js/'+styler+'.min.js"></script>\n'+
  '<script type="text/javascript" language="javascript" src="./DataTables/media/js/jquery.dataTables.min.js">  </script>\n'+
  '<script type="text/javascript" language="javascript" src="./DataTables/media/js/dataTables.'+styler+'.min.js">  </script>\n'+
  '<script type="text/javascript" language="javascript" src="./DataTables/extensions/Select/js/dataTables.select.min.js">  </script>\n'+
  '<script type="text/javascript" language="javascript" src="./DataTables/extensions/Buttons/js/dataTables.buttons.min.js">  </script>\n'+
  '<script type="text/javascript" language="javascript" src="./DataTables/extensions/Buttons/js/buttons.flash.min.js">  </script>\n'+
  '<script type="text/javascript" language="javascript" src="./DataTables/extensions/Buttons/js/buttons.html5.min.js">  </script>\n'+
  '<script type="text/javascript" language="javascript" src="./DataTables/extensions/Buttons/js/buttons.print.min.js">  </script>\n'
  );

/////////////////////////////////////////////////////////////////////////////
var mSelecTable = function  (args) 
/////////////////////////////////////////////////////////////////////////////
{
  var div = document.getElementById(args.container);  
  if (typeof(args.active)==='undefined') args.active = false;
//if (typeof(args.width)==='undefined') args.width = 800;
//if (typeof(args.height)==='undefined') args.height = 600;
  if (typeof(args.scrollY)==='undefined') args.scrollY = '82vh';
  if (typeof(args.autoWidth)==='undefined') args.autoWidth = true;
  if (typeof(args.dom)==='undefined') args.dom = 'Bft';
  if (typeof(args.hiddenCols)==='undefined') args.hiddenCols = [];  
  if (typeof(args.buttons)==='undefined') args.buttons = 
  ['csv','excel','copy','print'];
  if (typeof(args.enabledButtons)==='undefined') args.enabledButtons = [];
  if (typeof(args.cssTableClassModifiers)==='undefined') args.cssTableClassModifiers = '';
  if (typeof(args.cssTableClassBase)==='undefined') args.cssTableClassBase = 
    'table table-condensed table-striped table-bordered';
  if (typeof(args.prepare)==='undefined') args.prepare = function() {};
  if (typeof(args.allowAllKeyWhenEmpty)==='undefined') args.allowAllKeyWhenEmpty = false;
  if (typeof(args.keyFieldIdx)==='undefined') args.keyFieldIdx = -1;
  if (typeof(args.descFieldIdx)==='undefined') args.descFieldIdx = -1;
//console.log("TRACE: new mSelecTable");
//console.log(args);
  this.container = args.container;
  this.active = args.active;
//this.width = args.width;
//this.height = args.height;
  this.autoWidth = args.autoWidth;
  this.name = args.name;
  this.keyFieldIdx = args.keyFieldIdx;
  this.descFieldIdx = args.descFieldIdx;
  this.hiddenCols = args.hiddenCols;  
  this.url = args.url;
  this.scrollY = args.scrollY;
  this.dom = args.dom;
  this.buttons = args.buttons;
  this.buttonsIdx = {};
  for(i=0; i<this.buttons.length; i++)
  {
    if(typeof(this.buttons[i])=="string")
    {
      this.buttonsIdx[this.buttons[i]] = i;
    }
    else
    {
      this.buttonsIdx[this.buttons[i].text] = i;
    }
  }
  this.enabledButtons = args.buttons;
  this.cssTableClassModifiers = args.cssTableClassModifiers;
  this.cssTableClassBase = args.cssTableClassBase;
  this.allowAllKeyWhenEmpty = args.allowAllKeyWhenEmpty;
  this.divId = this.name+'Div';
  this.tabId = this.name+'Tab';
  this.json = '';
  this.cols = 0;
  this.rows = 0;
  this.colIdx = {};
  this.colNam = [];
  this.dttbl = {};
  this.selDat = [];
  this.selIdx = [];
  this.selKey = [];
  this.selDesc = [];
  this.allKey = [];
};
 
 /*
function resizeTableWhenDiv(div, table)
{
  table.height() 
});
*/ 
/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.html = function()
/////////////////////////////////////////////////////////////////////////////
{
  var div = document.getElementById(this.container);  
  var htm = 
  '<div id="'+this.divId+'" style="'+
  'width:100%; '+
  'height:100%; '+
//'width:'+div.offsetWidth+'px; '+
//'height:'+div.offsetHeight+'px; '+
  'display:'+(this.active?'block':'none')+
  '">\n'+ 
  '  <table id="'+this.tabId+'" class="'+
  this.cssTableClassBase+' '+
  this.cssTableClassModifiers+
  '" cellspacing="0" width="100%" height="100%"></table>\n'+
  '</div>\n';
//console.log('TRACE htm='+htm);
  return(htm);
};
 
/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.checkExistSerSel = function()
/////////////////////////////////////////////////////////////////////////////
{
  var exSel = existsSeriesSelection();
  this.buttons[this.buttonsIdx["Query"]].enabled = exSel;
  this.buttons[this.buttonsIdx["Summary"]].enabled = exSel;
  this.buttons[this.buttonsIdx["Table"]].enabled = exSel;
  this.buttons[this.buttonsIdx["Chart"]].enabled = exSel;
};
 
/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.show = function()
/////////////////////////////////////////////////////////////////////////////
{
//console.log("TRACE: mSelecTable.prototype.show:"+this.name+" -> "+typeof(this.dttbl.draw));
//console.log("TRACE: typeof(this.dttbl.draw):"+typeof(this.dttbl.draw));
  if (!(typeof(this.dttbl.draw)==='undefined'))
  {
  /*var div = document.getElementById(this.divId);  
  //console.log(this.dttbl);
  //console.log(this.dttbl.settings());
    this.dttbl.scrollY = div.offsetHeight - 50;
    $('.dataTables_scrollBody').css('height', div.offsetHeight - 50);
  //console.log("TRACE: new this.dttbl.scrollY="+this.dttbl.scrollY);
*/    
    this.checkExistSerSel();
    this.active = true;
  //this.dttbl.columns.adjust().draw();
    $("#"+this.divId).show();
    this.dttbl.draw();
  }
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.resize = function()
/////////////////////////////////////////////////////////////////////////////
{
//console.log("TRACE: mSelecTable.prototype.show:"+this.name+" -> "+typeof(this.dttbl.draw));
  this.show();
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.hide = function()
/////////////////////////////////////////////////////////////////////////////
{
  if (!(typeof(this.dttbl.draw)==='undefined'))
  {
    this.active = false;
    $("#"+this.divId).hide();
  }
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.getNumberOfColumns = function()
/////////////////////////////////////////////////////////////////////////////
{
  return(this.cols)
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.getNumberOfRows = function()
/////////////////////////////////////////////////////////////////////////////
{
  return(this.rows)
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.getColumnLabel = function(j)
/////////////////////////////////////////////////////////////////////////////
{
  return(this.json.columns[j].title)
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.exportData = function()
/////////////////////////////////////////////////////////////////////////////
{
//console.log(this.dttbl.buttons);
  var data = this.dttbl.buttons.exportData();
//console.log(data);
  var table = data.header.concat(data.body)
  return(table);
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.getFormattedValue = function(i,j,attr)
/////////////////////////////////////////////////////////////////////////////
{
  return(this.json.data[i][j])
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.filter = function(colIdx,values)
/////////////////////////////////////////////////////////////////////////////
{
  this.dttbl.column(colIdx)
    .search(values,true)
    .draw();
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.onReady = function(env)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE mSelecTable.prototype.onReady('+this.name+')');
  var div = document.getElementById(this.container);
  div.insertAdjacentHTML('beforeend', this.html());
  var this_ = this;
  
  var selectStyle = "os";
  if( isMobile.any() ) { selectStyle = "multi"; } 
  
//console.log(this_);
  env.ajax({
    url: this_.url,
    dataType: "json",
    error: function(result) {
  //console.log('TRACE ajax.error('+this_.name+','+this_.url+')');
    //console.log(this_);
    },
    success: function( json ) {
    //console.log('TRACE ajax.success('+this_.name+','+this_.url+')');
      json.select         = { style: selectStyle };
      json.scrollCollapse = false;
      json.paging         = false;
      json.info           = false;
      json.scrollY        = this_.scrollY;
      json.buttons        = this_.buttons;
      json.dom            = this_.dom;
      json.autoWidth      = this_.autoWidth;
      this_.json = json;
      this_.cols = this_.json.columns.length;
      this_.rows = this_.json.data.length;
      for(i=0; i<this_.cols; i++)
      {
        var cn = json.columns[i].title;
        this_.colIdx[cn] = i;
        this_.colNam.push(cn);
      }
      this_.allKey = [];
      
      if(this_.keyFieldIdx>=0 && this_.allowAllKeyWhenEmpty)
      {
        for (i = 0; i < json.data.length; i++) 
        { 
          this_.allKey[i] = json.data[i][this_.keyFieldIdx]; 
        }
      }
      console
      this_.dttbl = env("#"+this_.tabId).DataTable( json );
      for(i=0; i<this_.hiddenCols.length; i++)
      {
      //console.log('TRACE hidding column '+this_.name+'.'+this_.hiddenCols[i]);
      //var column = this_.dttbl.columns([{name:this_.hiddenCols[i]}]);
        var column = this_.dttbl.column('#'+this_.hiddenCols[i]);
      //console.log(column);
        column.visible(false);      
      }
      this_.dttbl.draw();
      document.getElementById(this_.divId).style.fontSize = "16px";
      //$("#dataTables_scroll").css({'font-size':'8px'});
    //this_.dttbl.columns.adjust().draw();
    //console.log(this);
    }
  });
	
  $("#"+this_.divId).on( 'click', 'tr', function () {
  //console.log(this_.getColumnLabel(0));
  //console.log(this_.getFormattedValue(4,0));
    var rows = this_.dttbl.rows( '.selected' ).indexes();
    var data = this_.dttbl.rows( rows ).data();
    this_.selDat = [];
    this_.selIdx = [];
    this_.selKey = [];
    this_.selDesc = [];
    for (i = 0; i < rows.length; i++) 
    { 
      this_.selDat[i] = data[i];
      this_.selIdx[i] = rows[i];
      if(this_.keyFieldIdx>=0)
      {
        this_.selKey[i] = data[i][this_.keyFieldIdx]; 
      }
      if(this_.descFieldIdx>=0)
      { 
        this_.selDesc[i] = data[i][this_.descFieldIdx]; 
      }
    }
    this_.checkExistSerSel();
  //console.log("TRACE: this_.keyFieldIdx:"+this_.keyFieldIdx);    
  //console.log("TRACE: this_.descFieldIdx:"+this_.descFieldIdx);    
  //console.log(this_.selDat);    
  //console.log(this_.selIdx);    
  //console.log(this_.selKey);    
  //console.log(this_.selDesc);   
    
  } );

};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.getSelOrAllKeys= function(allIfEmpty)
/////////////////////////////////////////////////////////////////////////////
{
  if(this.allowAllKeyWhenEmpty)
  {
  //console.log(this.name+'.getSelOrAllKeys('+allIfEmpty+')');    
  //console.log(this.allKey);    
  };
  if (typeof(allIfEmpty)==='undefined') allIfEmpty = false;
  if(!this.selKey.length && allIfEmpty) 
  { return(this.allKey); }
  else                    
  { return(this.selKey); }
}

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.selKeyStr = function(sep, allIfEmpty)
/////////////////////////////////////////////////////////////////////////////
{
  var sel = this.getSelOrAllKeys(allIfEmpty);
  if(!sel.length) { return(''); }
  else            { return(sel.join(sep)); }
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.selKeySqlList = function(allIfEmpty)
/////////////////////////////////////////////////////////////////////////////
{
  var sel = this.getSelOrAllKeys(allIfEmpty);
  if(!sel.length) { return(''); }
  else            
  { 
    return(sel.join(',')); 
  }
};

/////////////////////////////////////////////////////////////////////////////
mSelecTable.prototype.remove = function()
/////////////////////////////////////////////////////////////////////////////
{
  $("#"+this.divId).remove();  
};


