/////////////////////////////////////////////////////////////////////////////
//FILE: HyperCubeBrowser.js
//
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
function addTableFromUrl(args)  
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE addTableFromUrl('+args.pageName+')');
  var urlArgs = '';
  for(i=0; i<args.urlArgs.length; i++)
  {
    urlArgs += "&"+args.urlArgs[i].name+"="+
      encodeURIComponent(args.urlArgs[i].value);
  }
  var dp = new mSelecTable({
    active : args.isActive,
  //width: divWidth(),
  //height: divHeight(),
    container : 'HyperCubeBrowserDiv',
    name : args.pageName,
    keyFieldIdx : args.keyFieldIdx,
    descFieldIdx : args.descFieldIdx,    
    hiddenCols : args.hiddenCols,
    allowAllKeyWhenEmpty: args.allowAllKeyWhenEmpty,
    buttons : args.buttons,
    enabledButtons: args.buttons,
    url : "./dbTabJson.php?prj="+projectIdentifier()+"&tabEngine="+encodeURIComponent('DataTable')+
          urlArgs,
    dom : 'Bft',
    cssTableClassModifiers : '!nowrap'
  });
  divPagerMain.addPage(dp);
  return(dp)
};

/////////////////////////////////////////////////////////////////////////////
function addTableFromQuery(args)  
/////////////////////////////////////////////////////////////////////////////
{
  args.urlArgs = [
  { name:'dbtask', value:'query'},
  { name:'dbquery', value:args.query } ];
  return(addTableFromUrl(args));
};

/////////////////////////////////////////////////////////////////////////////
function addChartFromUrl(args)  
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE addChartFromUrl('+args.pageName+')');
  var urlArgs = '';
  for(i=0; i<args.urlArgs.length; i++)
  {
    urlArgs += "&"+args.urlArgs[i].name+"="+
      encodeURIComponent(args.urlArgs[i].value);
  }
  var dp = new seriesChart({
    active : args.isActive,
  //width: divWidth(),
  //height: divHeight(),
    container : 'HyperCubeBrowserDiv',
    name : args.pageName,
    buttons : args.buttons,
    enabledButtons: args.buttons,
    url : "./dbTabJson.php?prj="+projectIdentifier()+"&tabEngine="+encodeURIComponent('DyGraphNative')+
      urlArgs
  });
  divPagerMain.addPage(dp);
  return(dp);
};

/////////////////////////////////////////////////////////////////////////////
function addShowInfo(args)  
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE addShowInfo('+args.pageName+')');
  var dp = new showInfo({
    active : args.isActive,
  //width: divWidth(),
  //height: divHeight(),
    container : 'HyperCubeBrowserDiv',
    name : args.pageName,
    buttons : args.buttons,
    enabledButtons: args.buttons,
    html : args.html
  });
  divPagerMain.addPage(dp);
  return(dp);
};

/////////////////////////////////////////////////////////////////////////////
function classifyByDataset(args_)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE classifyByDataset');
//console.log(args_);
  var json_ = $.ajax({
    url: "./dbTabJson.php?prj="+projectIdentifier()+"&tabEngine="+
    encodeURIComponent('DataTable')+
    "&dbtask=query&dbquery="+encodeURIComponent(args_.query),
    contentType: "charset=utf-8",  
    dataType:'json',
    async: false}).responseText;
//console.log(json_);
  var table_ = JSON.parse(json_);
//console.log(table_);
  var dsList_ = {};
  var rows = table_.data.length;
  for(var i=0; i < rows; i++)
  {
    var dataset = table_.data[i][args_.datasetFieldIdx];
    var target = table_.data[i][args_.targetfieldIdx];
  //console.log('TRACE dataset:'+dataset+' target:'+target)
    if (typeof(dsList_[dataset])==='undefined') { dsList_[dataset] = []; }
    dsList_[dataset].push(target);
  }; 
  var result = {
    args:args_,
    json:json_,
    table:table_,
    dsList:dsList_,
    getTarget: function (dataset, sep) 
    {
      return(dsList[dataset].join(sep));
    }
  };
  return(result);
};
     
var ds_dim = classifyByDataset({
datasetFieldIdx:0, 
targetfieldIdx:1,
query: '                           \
SELECT co_dataset, co_dimension    \
FROM def_serie_key                 \
GROUP BY co_dataset, co_dimension  \
ORDER BY co_dataset, co_dimension  '
});   
  
var ds_key = classifyByDataset({
datasetFieldIdx:0, 
targetfieldIdx:1,
query:'                                           \
SELECT co_dataset,                                \
concat(co_dimension, \'.\', co_key) as co_dimkey  \
FROM def_serie_key                                \
GROUP BY co_dataset, co_dimension, co_key         \
ORDER BY co_dataset, co_dimension, co_key         '
});

/////////////////////////////////////////////////////////////////////////////
function existsSeriesSelection()
/////////////////////////////////////////////////////////////////////////////
{
  var datasets = divPagerMain.getPage("Datasets").selKeySqlList(false);
  var keyValues = divPagerMain.getPage("Keys").selKeySqlList(false);
  return((datasets.length>0)&&(keyValues.length>0));
};
  
/////////////////////////////////////////////////////////////////////////////
function getSeriesSelection()
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE listQuery:\n'+listQuery);
  var datasets = divPagerMain.getPage("Datasets").selKeySqlList(false);
//console.log(datasets); 
  var keyValues = divPagerMain.getPage("Keys").selKeySqlList(false);
  var listQuery = "";
  var sqlHtml = "";
  var tolHtml = "";
//console.log(keyValues); 
  var isOk = true;
  var errorMsg = ""
  if((datasets.length==0)&&(keyValues.length==0))
  {
    errorMsg = 'Sorry: You must select at least one or more datasets and one or more key values';
    isOk = false;
  }
  else if((datasets.length==0)&&(keyValues.length>0))
  {
    errorMsg = 'Sorry: You must select at least one or more datasets';
    isOk = false;
  }
  else if((datasets.length>0)&&(keyValues.length==0))
  {
    errorMsg = 'Sorry: You must select at least one or more key values';
    isOk = false;
  }
  else
  {
    var ds = divPagerMain.getPage("Datasets").selKey;
    var desc_ds = divPagerMain.getPage("Datasets").selDesc;
  //console.log(ds); 
    var kv = divPagerMain.getPage("Keys").selKey;    
    var desc_kv = divPagerMain.getPage("Keys").selDesc;    
  //console.log(kv); 
    var dkv = {};
    for(i=0; i<kv.length; i++)
    {
      var dk = kv[i].split(".")[0];
      if (typeof(dkv[dk])==='undefined') dkv[dk] = [];
      dkv[dk].push(kv[i]);
    }
  //console.log(dkv); 
    var condition = '';

    tolHtml = 
    "<h3>TOL Expression</h3>\n"+
    "<pre>\n"+
    "<font face='monospace' color='green'>"+
    Array(81).join("/")+"\n"+
    "//<b><i>HyperCube Query description</i></b>\n"+
    "//<b>Datasets</b>:";
    for(i=0; i<ds.length; i++)
    {
      if(i>0) { tolHtml += ","; }
      var title = "//  <b>"+ds[i]+"</b>: ";
      tolHtml += "\n"+title+
      $.trim(wordWrap(Array(title.length-5).join(" ")+desc_ds[i],74,"\n//    "));
    }
    tolHtml += "\n//<b>Keys</b>:";
    for (i=0; i < kv.length; i++)
    {
      if(i>0) { tolHtml += ","; }
      var tok = kv[i].split(".");
      var title = "//  <b>Dimension</b> <i>"+tok[0]+"</i>="+tok[1]+": ";
      tolHtml += "\n"+title+
      $.trim(wordWrap(Array(title.length-5).join(" ")+desc_kv[i],74,"\n//    "));
    }
    tolHtml += "\n"+Array(81).join("/")+"</font>\n"+
    "<b>Set</b> series = { olap::getMatchingSeries([[";
    for(i=0; i<ds.length; i++)
    {
      if(i>0) { tolHtml += "]],"; }
      if(condition!='') 
      { 
        condition += '\n  OR'; 
      }
      var dsi = ds[i];
      console.log('TRACE Dataset '+dsi); 
      var dsd = ds_dim.dsList[dsi];
      console.log('TRACE Dimensions '+dsd); 
      var cnd_ds = "";
      var tolHtml_ds = "";
      for(j=0; j<dsd.length; j++)
      {
        var dsdj = dsd[j];
        var dkvj = dkv[dsdj];
        console.log('TRACE dsdj:'+dsdj);
        console.log('TRACE dkvj:'+dkvj);
        if(!(typeof(dkvj)==='undefined'))
        {
          tolHtml_ds += "\n  [[ <b>Text</b> Dataset=<font color='blue'>\""+dsi+"\"</font>";
          console.log('TRACE dkvj:'+dkvj);
          for(k=0; k<dkvj.length; k++)
          {
            var tok = dkvj[k].split(".");
            tolHtml_ds += "; <b>Text</b> "+tok[0]+"=<font color='blue'>\""+tok[1]+"\"</font>";
          }
          var cnd = "id_serie in ("+
            "SELECT id_serie FROM view_dataset_dim_key_serie "+
            "WHERE co_dataset ='"+dsi+"' "+
            "AND co_dimension || '.' || co_key in ('"+dkvj.join("','")+"'))";
          if(cnd_ds!='') 
          { 
            cnd_ds += '\n    AND '; 
          }
          cnd_ds += cnd;
        //console.log('TRACE cnd:'+cnd);
        }
      }      
      if(cnd_ds=="") 
      {
        cnd_ds = "id_serie in ("+
          "SELECT id_serie FROM view_dataset_dim_key_serie "+
          "WHERE co_dataset ='"+dsi+"') ";
        tolHtml_ds = "\n  [[ <b>Text</b> Dataset=<font color='blue'>\""+dsi+"\"</font>";
      }
      condition += "("+cnd_ds+")";
      tolHtml += tolHtml_ds;
    }
    tolHtml += "]] ]] ) };\n";
    tolHtml += "</pre>\n";

    listQuery = "SELECT id_serie, co_serie, co_dataset \n"+
    "FROM  def_serie\n"+
    "WHERE \n"+condition;
    console.log('TRACE listQuery:'+listQuery);
    sqlHtml = 
    "<h3>SQL Expression</h3>\n"+
    "<pre>\n"+
    listQuery+
    "</pre>\n";    

  }
  var result = {
    Datasets : datasets,
    KeyValues : keyValues,
    ListQuery : listQuery,
    SqlHtml : sqlHtml,
    TolHtml : tolHtml,
    IsOk : isOk,
    ErrorMsg : errorMsg
  };
//console.log("TRACE: getSeriesSelection")
//console.log(result)
  return(result)
};
  
var pagePreparer = 
{
  Datasets : function(pageName) 
  { 
    divPagerMain.removePage('Summary');
    divPagerMain.removePage('Table');
    divPagerMain.removePage('Chart');
    divPagerMain.show(pageName);
  },
  Dimensions : function(pageName)
  {
    var selDataset = divPagerMain.getPage("Datasets").selKey;
  //console.log(selDataset); 
    var selDim = [];
    for(i=0; i<selDataset.length; i++)
    {
    //console.log(selDataset[i]); 
      var dim = ds_dim.dsList[selDataset[i]];
    //console.log(dim); 
      selDim = selDim.concat(dim);
    }
    var sel = selDim.join('|');
  //console.log(selDim); 
  //console.log(sel); 
    divPagerMain.removePage('Summary');
    divPagerMain.removePage('Table');
    divPagerMain.removePage('Chart');
    divPagerMain.getPage(pageName).filter(0,sel)
    divPagerMain.show(pageName);
  },
  Keys : function(pageName)
  {
    var selDataset = divPagerMain.getPage("Datasets").selKey;
  //console.log(selDataset); 
    var selKey = [];
    for(i=0; i<selDataset.length; i++)
    {
    //console.log(selDataset[i]); 
      var dim = ds_key.dsList[selDataset[i]];
    //console.log(dim); 
      selKey = selKey.concat(dim);
    }
    var sel = selKey.join('|');
  //console.log(selKey); 
  //console.log(sel); 
    divPagerMain.removePage('Summary');
    divPagerMain.removePage('Table');
    divPagerMain.removePage('Chart');
    divPagerMain.getPage(pageName).filter(4,sel)
    divPagerMain.show(pageName);
  },
  Query : function(pageName)
  {
    var serSel = getSeriesSelection();
    if(serSel.IsOk)      
    {
      addShowInfo({
        pageName:pageName,
        isActive:true,
        buttons:['Datasets','Dimensions','Keys','Query','Summary','Table','Chart'],
        enabledButtons:[],        
        html: serSel.TolHtml + serSel.SqlHtml
      });       
      divPagerMain.removePage('Table');
      divPagerMain.removePage('Chart');
      divPagerMain.getPage(pageName).onReady($);
      divPagerMain.show(pageName);
    }
    else
    {
      alert(serSel.ErrorMsg);
      divPagerMain.show("Datasets");
    }
  },
  Summary : function(pageName)
  {
  //console.log("TRACE: Summary 1")
    var serSel = getSeriesSelection();
  //console.log("TRACE: Summary 2 serSel.IsOk="+serSel.IsOk)
    if(serSel.IsOk)      
    {
    //console.log("TRACE: Summary 3")
      addTableFromUrl({
        pageName:"Summary",
        isActive:true,
        keyFieldIdx:0,
        allowAllKeyWhenEmpty:true,
        buttons:allButtons,
        enabledButtons:[],
        urlArgs:[
          { name:'dbtask', value:'seriesSummary' },
          { name:'listQuery', value:serSel.ListQuery },
        //{ name:'datasets', value:datasets },
        //{ name:'keyValues', value:keyValues },
        ]});
    //console.log("TRACE: Summary 3")
      divPagerMain.removePage('Table');
      divPagerMain.removePage('Chart');
      divPagerMain.getPage(pageName).onReady($);
    //console.log("TRACE: Summary 4")
      divPagerMain.show(pageName);
    //console.log("TRACE: Summary 5")
    }
    else
    {
      alert(serSel.ErrorMsg);
      divPagerMain.show("Datasets");
    }
  },
  Table : function(pageName)
  {
    if(!divPagerMain.existsPage("Summary"))
    {
      alert('Sorry: You must click Summary button before '+pageName);
      divPagerMain.show("Summary");
    }
    else
    {
      var serCodes = divPagerMain.getPage("Summary").selKeySqlList(true);
    //console.log('TRACE serCodes:'+serCodes);    
      addTableFromUrl({
        pageName:pageName,
        isActive:true,
        keyFieldIdx:0,
        buttons:allButtons,
        enabledButtons:[],
        urlArgs:[
         { name:'dbtask', value:'seriesDataFromCodes' },
         { name:'serCodes', value:serCodes }
        ]
      });
      divPagerMain.removePage('Chart');
      divPagerMain.getPage(pageName).onReady($);
      divPagerMain.show(pageName);
    } 
  },
  Chart : function(pageName)
  {
    if(!divPagerMain.existsPage("Summary"))
    {
      alert('Sorry: You must click Summary button before '+pageName);
      divPagerMain.show("Summary");
    }
    else
    {
      var serCodes = divPagerMain.getPage("Summary").selKeySqlList(true);
    //console.log('TRACE serCodes:'+serCodes);    
      addChartFromUrl({
        pageName:"Chart",
        isActive:true,
        buttons:['Datasets','Dimensions','Keys','Query','Summary','Table'],
        enabledButtons:[],        
        urlArgs:[
         { name:'dbtask', value:'seriesDataFromCodes' },
         { name:'serCodes', value:serCodes }
        ]
      });   
      divPagerMain.removePage('Table');
      divPagerMain.getPage(pageName).onReady($);
      divPagerMain.show(pageName);
    }
  }
};


/////////////////////////////////////////////////////////////////////////////
function clickPageButton(pgn)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE clickPageButton(pgn='+pgn+')');
  pagePreparer[pgn](pgn);
};

/////////////////////////////////////////////////////////////////////////////
function createPageButton(divPagerMain,pageId,enabled)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE createPageButton('+divPagerMain.container+","+pageId+')');
  var buttonInfo = { 
    text: pageId,
    action: function ( e, dt, node, config ) 
    {
    //console.log('TRACE createPageButton:action('+config.text+')');
      clickPageButton(config.text);
    },
    enabled: enabled 
  };
  return(buttonInfo);
};


var divPagerMain = new divPager({
  container : 'HyperCubeBrowserDiv'  
});  

var pageButtons = {
  Datasets:   createPageButton(divPagerMain,'Datasets',   true),
  Dimensions: createPageButton(divPagerMain,'Dimensions', true),
  Keys:       createPageButton(divPagerMain,'Keys',       true),
  Query:      createPageButton(divPagerMain,'Query',      true),
  Summary:    createPageButton(divPagerMain,'Summary',    true),
  Table:      createPageButton(divPagerMain,'Table',      true),
  Chart:      createPageButton(divPagerMain,'Chart',      true)
};

var datatableButtons = [
  'csv', 
  'excel', 
  'copy', 
  'print'
];

var allButtons = [
  pageButtons.Datasets,
  pageButtons.Dimensions,
  pageButtons.Keys,
  pageButtons.Query,
  pageButtons.Summary,
  pageButtons.Table,
  pageButtons.Chart
].concat(datatableButtons);

function selPageButtons(buttonList)
{
  var sel = [];
  for(i=0; i<pageButtons.length; i++)
  {
    var found = buttonList[pageButtons[i].text];
    if(!(typeof(found)==='undefined'))
    {
      sel.push(pageButtons[i]);
    }
  }
};

addTableFromQuery({
  pageName:"Datasets",
  isActive:true,
  keyFieldIdx:0,
  descFieldIdx:5,
  buttons:allButtons,
  enabledButtons:['Datasets'],
  query:"SELECT "+
  "co_dataset as Name,"+
  "co_source as Source,"+
  "co_magnitude ||\' [\'||co_units_tol||\']\' as Magnitude,"+
  "co_base_dating as Freq, "+
  "replace(te_dimensions,',',', ') as Dimensions,"+
  "te_description as Description "+
  "FROM def_dataset "+
  "ORDER BY id_dataset"});

addTableFromQuery({
  pageName:"Dimensions",
  isActive:false,
  keyFieldIdx:0,
  descFieldIdx:1,
  buttons:allButtons,
  enabledButtons:['Datasets','Dimensions'],
  query:"SELECT "+
  "co_dimension as Name,"+
  "te_desc as Description "+
  "FROM def_dimension "+
  "ORDER BY co_dimension"});        

addTableFromQuery({
  pageName:"Keys",
  isActive:false,
  keyFieldIdx:4,
  descFieldIdx:3,
  buttons:allButtons,
  enabledButtons:['Datasets','Dimensions','Keys'],
  query:"SELECT "+
  "co_dimension as Dimension,"+
  "co_name as Name, "+
  "co_key as Code, "+
  "te_description as Description,  "+
  "concat(co_dimension,'.',co_key) as co_dimkey \n"+  
  "FROM def_key "+
  "ORDER BY co_dimension,co_key",
  hiddenCols:['co_dimkey']});

jQuery( document ).ready( function( $ ) 
{ 
  divPagerMain.onReady($);
  divPagerMain.show("Datasets");
});

/* * /
$(window).resize(function()
{
//console.log('TRACE HyperCubeBrowser.onResize');
  divPagerMain.resizeCurrent();
});
/* */    

