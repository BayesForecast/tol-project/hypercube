<?php

include_once "general.php";

if($dbtask == 'TolExpression')
{
  echo getTolExpression($datasets, $keyValues);
}
else if($dbtask == 'seriesListSql')
{
  echo getSeriesListSql($dbms,$datasets, $keyValues);
}
else if($dbtask == 'seriesSummarySql')
{
  if($datasets!="") 
  {
    echo getSeriesSummarySql($dbms,$datasets, $keyValues);
  }
  else
  {
    echo getSeriesSummarySqlFromListQuery($dbms,$listQuery);
  }
}
else if($dbtask == 'getSeriesDataSqlFromCross')
{
  echo getSeriesDataSqlFromCross($dbms,$datasets, $keyValues);
}
else if($dbtask == 'query')
{
  echoJsonFromQuery($dbms,$tabEngine,$dbquery);
}  
else if($dbtask == 'seriesList')
{
  echoJsonFromQuery($dbms,$tabEngine,getSeriesListSql($dbms,$datasets, $keyValues));
}
else if($dbtask == 'seriesSummary')
{
  if($datasets!="") 
  {
    echoJsonFromQuery($dbms,$tabEngine,getSeriesSummarySql($dbms,$datasets, $keyValues));
  }
  else
  {
    echoJsonFromQuery($dbms,$tabEngine,getSeriesSummarySqlFromListQuery($dbms,$listQuery));
  }
}
else if($dbtask == 'seriesDataFromCross')
{
  echoJsonFromQuery($dbms,$tabEngine,getSeriesDataSqlFromCross($dbms,$datasets, $keyValues));
}
else if($dbtask == 'seriesDataFromCodes')
{
  echoJsonFromQuery($dbms,$tabEngine,getSeriesDataSqlFromCodes($dbms,$serCodes));
}
else if($dbtask == 'seriesSqlFromCodes')
{
  echo getSeriesDataFromQuery($dbms,$serCodes);
}

?>

